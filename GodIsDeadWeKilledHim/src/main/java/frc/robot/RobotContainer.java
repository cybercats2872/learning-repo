// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.DriveDistanceSparkMax;
import frc.robot.commands.DriveForwardTimed;
import frc.robot.commands.DriveWithJoysticks;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Limelight;
import frc.robot.subsystems.Arm;
import frc.robot.commands.ArmDown;
import frc.robot.commands.IntakeBall;
import frc.robot.commands.PIDAutoAim;
import frc.robot.commands.PIDAutoDistance;
import frc.robot.commands.ShootBall;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  private final DriveTrain driveTrain;
  private final DriveDistanceSparkMax driveDistance;
  private final DriveWithJoysticks drivewithJoystick;
  private final DriveForwardTimed driveforwardTimed;
  public static XboxController driverController;
  private final Intake intake;
  private final ShootBall shootBall;
  private final Arm arm;
  private final IntakeBall intakeBall;
  private final Limelight limeLight;
  // The robot's subsystems and commands are defined here...
  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    driveTrain = new DriveTrain();
    drivewithJoystick = new DriveWithJoysticks(driveTrain);
    drivewithJoystick.addRequirements(driveTrain);
    driveTrain.setDefaultCommand(drivewithJoystick);
    driveforwardTimed = new DriveForwardTimed(driveTrain);
    driveforwardTimed.addRequirements(driveTrain);
    driveDistance = new DriveDistanceSparkMax(driveTrain);
    driverController = new XboxController(Constants.JOYSTICK_NUMBER);
    intake = new Intake();
    shootBall = new ShootBall(intake);
    shootBall.addRequirements(intake);
    arm = new Arm();
    limeLight = new Limelight();
    intakeBall = new IntakeBall(intake, arm);
    intakeBall.addRequirements(intake, arm);
    driveTrain.setDefaultCommand(drivewithJoystick);
    // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    driveTrain.setDefaultCommand(drivewithJoystick);
    JoystickButton shootButton = new JoystickButton(driverController, XboxController.Button.kBumperLeft.value);
    shootButton.whileHeld(new ShootBall(intake));
    JoystickButton intakeButton = new JoystickButton(driverController, XboxController.Button.kBumperRight.value);
    intakeButton.whenHeld(new IntakeBall(intake, arm));
    JoystickButton armButton = new JoystickButton(driverController, XboxController.Button.kA.value);
    armButton.whileHeld(new ArmDown(arm));
    JoystickButton autoAimButton = new JoystickButton(driverController, XboxController.Button.kB.value);
    autoAimButton.whenPressed(new PIDAutoAim(driveTrain,limeLight));
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
   return driveDistance;
    // An ExampleCommand will run in autonomous
    //return m_autoCommand;
  }
}
