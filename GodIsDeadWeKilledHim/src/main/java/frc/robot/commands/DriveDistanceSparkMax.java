// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;
public class DriveDistanceSparkMax extends CommandBase {
  /** Creates a new DriveDistance. */
  private CANPIDController leftController;
  private CANPIDController rightController;
  private CANEncoder leftEncoder;
  private CANEncoder rightEncoder;
  public double kP, kI, kD, kIz, kFF, kMaxOutput, kMinOutput;
  DriveTrain driveTrain;

  public DriveDistanceSparkMax(DriveTrain dt) {
    driveTrain = dt;
    addRequirements(driveTrain);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    kP = 0.1; 
    kI = 1e-4;
    kD = 1; 
    kIz = 0; 
    kFF = 0; 
    kMaxOutput = 1; 
    kMinOutput = -1;
    leftController = driveTrain.getleftside().getPIDController();
    rightController = driveTrain.getrightside().getPIDController();
    leftEncoder = driveTrain.getleftside().getEncoder();
    rightEncoder = driveTrain.getrightside().getEncoder();
    leftController.setP(kP);
    leftController.setI(kI);
    leftController.setD(kD);
    leftController.setIZone(kIz);
    leftController.setFF(kFF);
    rightController.setP(kP);
    rightController.setI(kI);
    rightController.setD(kD);
    rightController.setIZone(kIz);
    rightController.setFF(kFF);
    leftController.setOutputRange(kMinOutput, kMaxOutput);
    rightController.setOutputRange(kMinOutput, kMaxOutput);
    SmartDashboard.putNumber("P Gain", kP);
    SmartDashboard.putNumber("Set Rotations", 0);
    SmartDashboard.putNumber("Max Output", 0);
    SmartDashboard.putNumber("Min Output", 0);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double p = SmartDashboard.getNumber("P Gain", 0);
    double i = SmartDashboard.getNumber("I Gain", 0);
    double d = SmartDashboard.getNumber("D Gain", 0);
    double iz = SmartDashboard.getNumber("I Zone", 0);
    double ff = SmartDashboard.getNumber("Feed Forward", 0);
    double max = SmartDashboard.getNumber("Max Output", 0);
    double min = SmartDashboard.getNumber("Min Output", 0);
    double rotations = SmartDashboard.getNumber("Set Rotations", 0);
    // if PID coefficients on SmartDashboard have changed, write new values to controller
    if((p != Constants.KP_DISTANCE)) { leftController.setP(p); rightController.setP(p); Constants.KP_DISTANCE= p; }
    if((i != kI)) { leftController.setI(i); rightController.setI(i); kI = i; }
    if((d != kD)) { leftController.setD(d); rightController.setD(d);kD = d; }
    if((iz != kIz)) { leftController.setIZone(iz); rightController.setIZone(iz);kIz = iz; }
    if((ff != kFF)) { leftController.setFF(ff); rightController.setFF(ff);kFF = ff; }
    if((max != kMaxOutput) || (min != kMinOutput)) { 
      leftController.setOutputRange(min, max);
      rightController.setOutputRange(min, max);
      kMinOutput = min; kMaxOutput = max; 
    }
    leftController.setReference(rotations, ControlType.kPosition);
    rightController.setReference(rotations, ControlType.kPosition);
    SmartDashboard.putNumber("SetPoint", rotations);
    SmartDashboard.putNumber("ProcessLeftVariable", leftEncoder.getPosition());
    SmartDashboard.putNumber("ProcessRightVariable", rightEncoder.getPosition());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
