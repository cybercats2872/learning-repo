// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Limelight;
// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class PIDAutoAim extends PIDCommand {
  /** Creates a new PIDAutoDistance. */
  public PIDAutoAim(DriveTrain dt, Limelight ll) {
    super(
        // The controller that the command will use
        new PIDController(1, 0, 0),
        // This should return the measurement
        ll::getX,
        // This should return the setpoint (can also be a constant)
        0,
        // This uses the output
        output -> { dt.diffDrive(output, -output);
          // Use the output here
        });
    // Use addRequirements() here to declare subsystem dependencies.
    // Configure additional PID options by calling `getController` here.
    //good job you're doing great love you :D 
    // hope you have a great day 
    // wow imagine not trusting me -_-
    // mhmmm very interesting shawn 
    // "ah did u tab just now"
    // its ok i forgive you cause im so great :)
    getController().enableContinuousInput(-29.8, 29.8);
    getController().setTolerance(.5);

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
