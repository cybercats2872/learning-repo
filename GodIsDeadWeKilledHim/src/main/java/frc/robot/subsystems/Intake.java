/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
public class Intake extends SubsystemBase {
  /**
   * Creates a new Intake.
   */
  private VictorSPX IntakeRoller;
  public Intake() {
    IntakeRoller = new VictorSPX(Constants.rollerMotor); 
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
  public void setIntakeRoller(double speed){
    IntakeRoller.set(ControlMode.PercentOutput, speed);
  }
  public void stop(){
    IntakeRoller.set(ControlMode.PercentOutput,0);
  }
}
