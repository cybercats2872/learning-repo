// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.Constants;
import com.revrobotics.CANSparkMax; 
public class DriveTrain extends SubsystemBase {
  private CANSparkMax leftMaster = new CANSparkMax(Constants.MotorLeft1ID, MotorType.kBrushless);
  private CANSparkMax leftSlave = new CANSparkMax(Constants.MotorLeft2ID, MotorType.kBrushless);
  private CANSparkMax rightMaster = new CANSparkMax(Constants.MotorRight1ID, MotorType.kBrushless);
  private CANSparkMax rightSlave = new CANSparkMax(Constants.MotorRight2ID, MotorType.kBrushless);
  
  private DifferentialDrive drive = new DifferentialDrive(leftMaster, rightMaster);
  

  /** Creates a new DriveTrain. */
  public DriveTrain() {
    leftSlave.follow(leftMaster);
    rightSlave.follow(rightMaster);
    leftMaster.setInverted(false);
    rightMaster.setInverted(false);
  }
  public void drivewithJoysticks(XboxController controller, double speed){
    drive.arcadeDrive(-controller.getRawAxis(Constants.XboxLeftYaxis)*speed, controller.getRawAxis(Constants.XboxRightXaxis)*speed);
  }
  public void driveforward(double speed){
    drive.tankDrive(speed, speed);
  }
  public void diffDrive(double leftSpeed, double rightSpeed){
    drive.tankDrive(leftSpeed, rightSpeed);
  }
  public CANSparkMax getleftside(){
    return leftMaster;
  }
  public CANSparkMax getrightside(){
    return rightMaster;
  }
  public void stop(){
    drive.stopMotor();
  }
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
