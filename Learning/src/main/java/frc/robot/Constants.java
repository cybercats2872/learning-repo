// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final int MotorLeft1ID = 3;
	public static final int MotorLeft2ID = 4;
	public static final int MotorRight1ID = 1;
	public static final int MotorRight2ID = 2;
	public static final int BagMotor = 7;
	public static final int rollerMotor = 9;
	public static final int XboxLeftYaxis = 1;
	public static final int XboxRightXaxis = 4;
	public static final double DRIVETRAINSPEED = 0.7;
	public static final double AUTONOMOUS_SPEED = 0;
	public static final double DRIVE_FORWARD_TIME = 3.0;
	public static final int JOYSTICK_NUMBER = 0;
	public static final double SHOOTER_SPEED = 1;
	public static final double ARMDOWN_SPEED = -0.4;
	public static final double INTAKE_SPEED = -1;
	public static final double SPIN_LEFT_SPEED = 0.3;
	public static final double SPIN_RIGHT_SPEED = -0.3;
	public static final double KP_STEERING = -0.09;
	public static final double KP_DISTANCE = 0.1;
	public static final double THRESHOLD = 0.5;
	public static final double GYRO_KP = 0.09;
	public static final double ROBOT_TRACKWIDTH = .577935440971381;
	public static final double WHEEL_SIZE = 0;
	public static final double WHEEL_DIAMATER = 6.0;
	public static final double KA = .439;
	public static final double KV = 2.83;
	public static final double KS = .111;
	public static final double LEFT_KP = 2.47;//2.31
	public static final double RIGHT_KP = 2.47;
	public static final double MAX_SPEED = 0;
	public static final double MAX_VELOCITY = 0;
}